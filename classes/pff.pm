package pff;
use strict;

sub new {
    my $self = {};
    # for "redirect" sub
    $self->{REDIRECT_URL} = undef;
    $self->{LOGIN} = undef;

    # for "module_find" sub
    # how many dirs back to get to the root. "0" for the same dir, "1" for 1 directory up
    $self->{LEVEL} = undef;
    # session variables
    $self->{SNAME} = undef;
    $self->{SID} = undef;
    # "spacer" is just for a beautiful html code
    $self->{SPACER} = undef;
    
    bless($self);
    return $self;
}

sub redirect {
    my $self = shift;
    if (@_) {
        $self->{REDIRECT_URL} = shift;
        $self->{LOGIN} = shift;
    }
    print "Content-type: text/html\n\n";
    print "<html>\n<head>\n";
    print "<meta http-equiv=\"REFRESH\" content=\"0;url=$self->{REDIRECT_URL}\">\n";
    print "</head>\n<body>\n";
    if ($self->{LOGIN}) {
        print "<h1>Please Log in</h1>\n";
    }
    print "</body>\n</html>";
}

sub module_find {
    my $self = shift;
    if (@_) {
        $self->{LEVEL} = shift;
        $self->{SNAME} = shift;
        $self->{SID} = shift;
        $self->{SPACER} = shift;
    }
    my $lvl = $self->{LEVEL};
    my $curr_mod;
    # if ($lvl > 0) then we are in a module directory, then its necessary to make the current module link active
    if ($lvl > 0) {
        open(CURRENT, "name.txt");
        $curr_mod = <CURRENT>;
        chomp($curr_mod);
        close(CURRENT);
    }
    my $mod_dir = "";
    my $lvl_down = "../";
    for (my $i = 0; $i < $lvl; $i++) {
        $mod_dir .= $lvl_down;
    }
    $mod_dir .= "modules";
    # open dir with modules
    opendir(MODULES, $mod_dir);
    # read what's inside it
    my @DIRS = grep(!/^\.\.?$/, readdir MODULES);
    closedir(MODULES);
    # were there any? actually, there must be something but in case of...
    my $arr_size = @DIRS;
    if ($arr_size == 0) { return 1; }
    # now dig into the dirs and link to html
    foreach(@DIRS) {
        my $dir = "$mod_dir/$_";
        opendir(DH, $dir);
        open(NAME, "$dir/name.txt");
        my $name = <NAME>;
        chomp($name);
        if ($curr_mod) {
            if ($name == $curr_mod) {
                print "$self->{SPACER}<li class=\"active\"><a href=\"#\" class=\"active\">$name</a></li>\n";
            }
            else {
                print "$self->{SPACER}<li><a href=\"/$dir/start.t?$self->{SNAME}=$self->{SID}\">$name</a></li>\n";
            }
        }
        else {
            print "$self->{SPACER}<li><a href=\"/$dir/start.t?$self->{SNAME}=$self->{SID}\">$name</a></li>\n";
        }

        close(NAME);
        closedir(DH);
    }
}

1;
