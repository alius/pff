#!/usr/bin/perl -w

# Copyright (c) 2007-2008 Artjom Vassiljev <me@al1us.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use strict;

use CGI;
use CGI::Session qw/-ip-match/;

# variables
my $cgi = CGI->new();
my $action = $cgi->param("action");
my $return = $cgi->param("return");
my $direction = $cgi->param("direction");
my $quick = $cgi->param("quick");
my @protocol = $cgi->param("protocol");
my ($proto, $all, $tcp, $udp);
my $from = $cgi->param("from");
my $port_from = $cgi->param("port_from");
my $to = $cgi->param("to");
my $port_to = $cgi->param("port_to");
my $num = $cgi->param("num");
my $q = $cgi->param("q");
my $pf_conf = "/etc/pf.conf";
my $line = 0;
my $title = "Add rules";
my $bridge_name = "sis0";

my $sid = $cgi->param("CGISESSID") || undef;
my $session = new CGI::Session(undef, $sid, {Directory=>"/tmp"});
my $sname = $session->name;

# let's check whether the user is logged in or not
my $logged = $session->param("logged");
if (!$logged) { &redirect("/start.t?$sname=$sid"); }
else {
    
    # if we have rule, then write it and redirect to the main page
    if ($action) {
        
        # is it short or normal rule
        if (!$q) {
            # parse the protocol array
            foreach(@protocol) {
                if ($_ eq "all") {
                    $all = "all";
                }
                elsif ($_ eq "tcp") {
                    $tcp = "tcp";
                }
                else {
                    $udp = "udp";
                }
            }
            
            # change the default values
            if ($all) { $proto = ""; }
            else {
                if ($tcp && !$udp) {
                    $proto = "proto tcp"
                }
                elsif (!$tcp && $udp) {
                    $proto = "proto udp"
                }
                elsif ($tcp && $udp) {
                    $proto = "proto {tcp,udp}"
                }
                else { $proto = ""}
            }
            
            if (!$from) {
                $from = "from any";
            }
            else {
                $from = "from " . $from;
            }
            
            if (!$to) {
                $to = "to any";
            }
            else {
                $to = "to " . $to;
            }
            
            if (!$quick) {
                $quick = "";
            }
            
            if (!$port_from) {
                $port_from = "";
            }
            else {
                $port_from = "port " . $port_from;
            }
            
            if (!$port_to) {
                $port_to = "";
            }
            else {
                $port_to = "port " . $port_to;
            }
            
            if (!$return) {
                $return = "";
            }
                
            # create the rule
            my $rule = "$action " . "$return " . "$direction " . "$quick " . "on $bridge_name " . "$proto " . "$from " . "$port_from " . "$to " . "$port_to";
            # if any of the operators were blank, then two whitespaces may occur.
            $rule =~ tr/  / /;
            
            &rule_print($rule);
        }
        else {
            # create the rule
            my $rule = "$action " . "$direction " . "on $bridge_name";
            
            &rule_print($rule);
        }
    
    }
    
    # if no rules, then display the add page
    else {
        &redirect("/modules/pf/");
    }
}
    
# functions

sub html {
    print "<html>\n";
    print "<head>\n";
    print "<title></title>\n";
    print "<script type=\"text/javascript\" src=\"/js/popup.js\"></script>\n";
    print "</head>\n";
    print "<body>\n";
    print "<br /><br /><br /><br /><br /><br /><br />\n";
    print "<center>";
    print "<a href=\"#\" onclick=\"javascript:exityes()\">Rule added. Press here to close the window.</a>";
    print "</center>\n";
    print "</body>\n";
    print "</html>\n";
}
    
sub redirect {
    my $address = $_[0];
    print "<html>\n<head>\n";
    print "<meta http-equiv=\"REFRESH\" content=\"0;url=$address\">\n";
    print "</head>\n<body>\n";
    print "</body>\n</html>";
}
    
sub rule_print {
    # mount filesystem for writing
    system("mount -o rw,noatime /dev/wd0a /");
    my $c_rule = $_[0];
    # open pf.conf for reading, read rules, close file, open again for writing
    open(PF, $pf_conf) or die "$!";
    my @PF_FILE = <PF>;
    close(PF);
        
    if (!$num) {
        open(OUT, ">>$pf_conf");
        print OUT "$c_rule\n";
        close OUT;
    }
    else {
        open(OUTFILE, ">$pf_conf");
        foreach(@PF_FILE) {
            # count lines;
            $line++;
            # if the next line is the rule we got, then write the buffered line and then the rule we received
            if ($num == $line+1) {
                print OUTFILE $_;
                # print the received rule
                print OUTFILE "$c_rule\n";
            }
            else {
                print OUTFILE $_;
            }
        }
        close(OUTFILE);
    }
    # mount read-only
    system("mount -o ro /dev/wd0a /");
    
    &html();
}

exit 0;
