#!/usr/bin/perl -w

# Copyright (c) 2007-2008 Artjom Vassiljev <me@al1us.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use lib 'classes/';
use pff;

use strict;

use CGI;
use CGI::Session qw/-ip-match/;;
use Expect;

my $pf2 = pff->new();
my $cgi = CGI->new();
my $sid = $cgi->param("CGISESSID") || undef;
my $session = new CGI::Session(undef, $sid, {Directory=>"/tmp"});
my $sname = $session->name;

# let's check whether the user is already logged on
my $logged = $session->param("logged");
if ($logged) { $pf2->redirect("/welcome.t?$sname=$sid"); }
else {
   my $username = $cgi->param("username");
   my $password = $cgi->param("password");
   
   # if no username was provided, print login form
   if (!$username) {
      &login_header();
      &login_form();
   }
   
   # if no password was provided, print login form
   elsif (!$password) {
      &login_header();
      print $cgi->font( { -color => "red" }, "No blank passwords!");
      &login_form();
   }
   
   # if username and password are ok, then display something
   
   else {
   
   # let's test, whether the user is ok or not
       my $status = &check_user($username,$password);
   
   # if the username or pass is incorrect - login again
       if ($status == 1) {
           &login_header();
           print $cgi->font( { -color => "red" }, "Bad username or password");
           &login_form();
       }
   
   # if everything is ok, then start the session
   # put all the params there and redirect to the main page
       else {
   #	$session = new CGI::Session(undef, undef, {Directory=>"/tmp"});
           $session->expire("+15m");
           $session->param("logged", 1);
           print $session->header();
           &redirect();
       }
   }
}

# functions

sub login_header {
   print "Content-type: text/html\n\n";
   print "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
   print "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n";
   print "<head>\n";
   print "    <title>start</title>\n";
   print "    <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\n";
   print "    <link rel=\"shortcut icon\" href=\"/images/favicon.ico\" />\n";
   print "    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/main.css\" media=\"screen\" />\n";
   print "    <script type=\"text/javascript\" src=\"/js/jquery.js\"></script>\n";
   print "</head>\n";
   print "<body>\n";
   print "<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />\n";
}

sub login_form {
   print "<div id=\"login_global\">\n";
   print "  <center>\n";
   print "     <form name=\"login\" action=\"\" method=\"post\">\n";
   print "          <input tabindex=\"1\" name=\"username\" type=\"text\" onfocus=\"if(this.value == 'Username') this.value = ''\" value=\"Username\" /><br />\n";
   print "          <input name=\"password\"  type=\"password\" onfocus=\"if(this.value == '********') this.value = ''\" value=\"********\" /><br />\n";
   print "          <input name=\"$sname\" type=\"hidden\" value=\"$sid\">\n";
   print "          <input accesskey=\"s\" id=\"login_btn\" style=\"\" value=\"Login\" type=\"submit\">\n";
   print "     </form>\n";
   print "  </center>\n";
   print "</div>\n";
   print "</body>\n";
   print "</html>\n";
}

sub check_user {
   my $u = $_[0];
   my $p = $_[1];
   my $timeout = 1;
   my $patt;
   
   my $exp = Expect->spawn("login") or die "Cannot spawn login: $!\n";

   $exp->expect($timeout, "Login: ");
   $exp->send("$u\n");
   $exp->expect($timeout, "Password: ");
   $exp->send("$p\n");
   $patt = $exp->expect($timeout, "Login incorrect");
   if ($patt) {
      return 1;
      $exp->hard_close();
   }
   else {
      return 0;
      $exp->hard_close();
   }
}

sub redirect {
   my $sname = $session->name;
   my $sid = $session->id;
   print "<html>\n";
   print "<head>\n";
   print "<meta http-equiv=\"REFRESH\" content=\"0;url=/welcome.t?$sname=$sid\">\n";
   print "</head>\n";
   print "<body>\n";
   print "</body>\n";
   print "</html>";
}

exit 0;
