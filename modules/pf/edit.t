#!/usr/bin/perl -w

# Copyright (c) 2007-2008 Artjom Vassiljev <me@al1us.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use strict;
use CGI;
use CGI::Session qw/-ip-match/;

# variables
my $cgi = CGI->new();
my $cmd = $cgi->param("cmd");
my $num = $cgi->param("num");
my $action = $cgi->param("action");
my $direction = $cgi->param("direction");
my $flag1 = $cgi->param("flag1");
my $flag2 = $cgi->param("flag2");
my $return = $cgi->param("return");
my $quick = $cgi->param("quick");
my $proto = $cgi->param("proto");
my $from = $cgi->param("from");
my $port_from = $cgi->param("port_from");
my $to = $cgi->param("to");
my $port_to = $cgi->param("port_to");
my $pf_conf = "/etc/pf.conf";

my $sid = $cgi->param("CGISESSID") || undef;
my $session = new CGI::Session(undef, $sid, {Directory=>"/tmp"});
my $sname = $session->name;

my $logged = $session->param("logged");
if (!$logged) { &redirect("/start.t?$sname=$sid"); }
else {
    if ($cmd eq "edit") { &edit($num) }
    elsif ($cmd eq "delete") { &del($num) }
    else { &redirect("start.t?$sname=$sid") }
}

# functions
sub edit {
    &html_header();
    &action_comment();
    &html_footer();
}

# help: del(line number)
sub del {
    # mount filesystem
    system("mount -o rw,noatime /dev/wd0a /");
    my $line_number = $_[0];
    my $line = 0;
    open(PF, "$pf_conf");
    my @PF_FILE = <PF>;
    close(PF);
    open(PF, ">$pf_conf");
    foreach (@PF_FILE) {
        $line++;
        if ($line_number == $line) {
            next;
        }
        else {
            print PF "$_";
        }
        
    }
    # mount read-only
    system("mount -o ro /dev/wd0a /");
    &redirect("start.t");
}

sub redirect {
    my $address = $_[0];
    print "Content-type: text/html\n\n";
    print "<html>\n<head>\n";
    print "<meta http-equiv=\"REFRESH\" content=\"0;url=$address\">\n";
    print "</head>\n<body>\n";
    print "</body>\n</html>";
}

sub html_header {
    print "Content-type: text/html\n\n";
    print "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
    print "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n";
    print "<head>\n";
    print "    <title>Edit</title>\n";
    print "    <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\n";
    print "    <link rel=\"shortcut icon\" href=\"/images/favicon.ico\" />\n";
    print "    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/main.css\" media=\"screen\" />\n";
    print "    <script type=\"text/javascript\" src=\"/js/jquery.js\"></script>\n";
    print "</head>\n";
    print "<body>\n";
    print "    <div id=\"global\">\n";
    print "        <div id=\"header\">\n";
    print "<form method=\"post\" action=\"\" style=\"float:right\">\n";
    print " <input type=\"hidden\" name=\"cmd\" value=\"logout\">\n";
    print " <input type=\"submit\" value=\"logout\">\n";
    print "</form>\n";
    print "            <span class=\"title\">BRIDGING FIREWALL.</span>\n";
    print "            <span class=\"sub_title\">The invisible security</span>\n";
    print "        </div>\n";
    print "        <div id=\"mainmenu\">\n";
    print "            <ul>\n";
    print "                <li><a href=\"/modules/pf/?$sname=$sid\">Packet filter</a></li>\n";
    print "                <li><a href=\"/modules/bridge/?$sname=$sid\" class=\"active\">Bridge</a></li>\n";
    print "            </ul>\n";
    print "        </div>\n\n";        
    print "        <div id=\"content\">\n";
    print "        <form name=\"save\" action=\"\" method=\"post\">\n";
    print "            <div id=\"sub_section\">\n";
    print "                <a href=\"form_add.t??$sname=$sid\" class=\"buttons\">add</a>\n";
    print "                <a href=\"#\" class=\"buttons\">save</a>\n";
    print "                <a href=\"#\" class=\"buttons\">remove</a>\n";
    print "            </div>\n";
    print "            <h2>Edit rule:</h2>\n";
    print "            <table>\n";
    print "                <thead>\n";
    print "                    <tr class=\"top\">\n";
    print "                        <td>#</td>\n";
    print "                        <td>action</td>\n";
    print "                        <td>return</td>\n";
    print "                        <td>direction</td>\n";
    print "                        <td>quick</td>\n";
    print "                        <td>proto</td>\n";
    print "                        <td>from</td>\n";
    print "                        <td>port</td>\n";
    print "                        <td>to</td>\n";
    print "                        <td>port</td>\n";
    print "                        <td>flags</td>\n";
    print "                    </tr>\n";
    print "                </thead>\n";
}

sub action_comment {
    open(PF, "$pf_conf");
    my @PF_FILE = <PF>;
    close(PF);
    my $comment = $PF_FILE[$num-1];
    # clear memory
    @PF_FILE = "";
    
    print "                    <tr>\n";
    print "                    <td><input type=\"text\" name=\"num\" value=\"$num\"></td>\n";
    print "                    <td><input type=\"text\" name=\"comment\" value=\"$comment\"></td>\n";
    print "                    <td></td>\n";
    print "                    <td></td>\n";
    print "                    <td></td>\n";
    print "                    <td></td>\n";
    print "                    <td></td>\n";
    print "                    <td></td>\n";
    print "                    <td></td>\n";
    print "                    <td></td>\n";
    print "                    <td></td>\n";
    print "                </tr>\n";
}

sub html_footer {
    print "                </table>\n";
    print "            <script>\n";
    print "                $(\"tr:nth-child(odd)\").addClass(\"dark\");\n";
    print "            </script>\n";
    print "        </div>\n";
    print "        <div id=\"sub_section\">\n";
    print "            <a href=\"form_add.t?$sname=$sid\" class=\"buttons\">add</a>\n";
    print "            <a href=\"edit.t?$sname=$sid&cmd=save&num=$num\" class=\"buttons\">save</a>\n";
    print "            <a href=\"edit.t?$sname=$sid&cmd=delete&num=$num\" class=\"buttons\">remove</a>\n";
    print "        </div>\n";
    print "    </div>\n";
    print "</body>\n";
    print "</html>\n";
}

exit 0;


