#!/usr/bin/perl -w

# Copyright (c) 2007-2008 Artjom Vassiljev <me@al1us.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use strict;

use CGI;
use CGI::Session qw/-ip-match/;

# variables
my $pf_conf = "/etc/pf.conf";
my $line = 0;

my $cgi = CGI->new();
my $sid = $cgi->param("CGISESSID") || undef;
my $session = new CGI::Session(undef, $sid, {Directory=>"/tmp"});
my $sname = $session->name;

# let's check whether the user is logged in or not
my $logged = $session->param("logged");
if (!$logged) { &redirect("/start.t?$sname=$sid"); }
else {

    # read conf from file
    open(PF, $pf_conf);
    my @PF_FILE = <PF>;
    
    my $size = @PF_FILE;
    chomp($size);
    
    close(PF);
    
    # print HTML header
    &html_header();
    
    foreach(@PF_FILE) {
        # count lines;
        $line++;
        &parse_rule($_);
    }
    
    # print HTML footer
    &html_footer();
    
}

# functions

sub html_header {
    print "Content-type: text/html\n\n";
    print "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
    print "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n";
    print "<head>\n";
    print "    <title>PF</title>\n";
    print "    <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\n";
    print "    <link rel=\"shortcut icon\" href=\"/images/favicon.ico\" />\n";
    print "    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/main.css\" media=\"screen\" />\n";
    print "    <script type=\"text/javascript\" src=\"/js/jquery.js\"></script>\n";
    print "    <script type=\"text/javascript\" src=\"/js/popup.js\"></script>\n";
    print "</head>\n";
    print "<body>\n";
    print "    <div id=\"global\">\n";
    print "        <div id=\"header\">\n";
    print "<form method=\"post\" action=\"\" style=\"float:right\">\n";
    print " <input type=\"hidden\" name=\"cmd\" value=\"logout\">\n";
    print " <input type=\"submit\" value=\"logout\">\n";
    print "</form>\n";
    print "            <span class=\"title\">BRIDGING FIREWALL.</span>\n";
    print "            <span class=\"sub_title\">The invisible security</span>\n";
    print "        </div>\n";
    print "        <div id=\"mainmenu\">\n";
    print "            <ul>\n";
    print "                <li><a href=\"/modules/bridge/start.t?$sname=$sid\">Bridge</a></li>\n";
    print "                <li class=\"active\"><a href=\"#\" class=\"active\">Packet filter</a></li>\n";
    print "            </ul>\n";
    print "        </div>\n";
    print "            <div id=\"content\">\n";
    print "            <div id=\"sub_section\">\n";
    print "                <a href=\"form_add.t?$sname=$sid\" class=\"buttons\" onClick=\"return popup(this, 'add')\">add</a>\n";
    print "            </div>\n";
    print "            <h2>Rules:</h2>\n";
    print "            <table>\n";
    print "                <thead>\n";
    print "                    <tr class=\"top\">\n";
    print "                        <td>#</td>\n";
    print "                        <td>action</td>\n";
    print "                        <td>return</td>\n";
    print "                        <td>direction</td>\n";
    print "                        <td>quick</td>\n";
    print "                        <td>proto</td>\n";
    print "                        <td>from</td>\n";
    print "                        <td>port</td>\n";
    print "                        <td>to</td>\n";
    print "                        <td>port</td>\n";
    print "                        <td>flags</td>\n";
    print "                        <td>edit</td>\n";
    print "                        <td>delete</td>\n";
    print "                    </tr>\n";
    print "                </thead>\n";
}

sub html_footer {
    print "            </table>\n";
    print "        </div>\n";
    print "        <div id=\"sub_section\">\n";
    print "            <a href=\"form_add.t?$sname=$sid\" class=\"buttons\" onClick=\"return popup(this, 'add')\">add</a>\n";
    print "        </div>\n";
    print "    </div>\n";
    print "</body>\n";
    print "</html>\n";
}

sub macro {
    if ($_[0] =~m/=/) {
        return 1;
    }
    else {
        return 0;
    }
}

sub comment {
    if ($_[0] =~ m/#/) {
        return 1;
    }
    else {
        return 0;
    }
}

sub parse_rule {
    # variables
    my ($proto, $tcp, $udp);
    my ($action, $return, $direction, $quick);
    my ($protocol, $from, $port_from, $to, $port_to, $flags);
    my $rule = $_[0];
    
    # let's see what we have: comment, macro or pf rule
    if (&comment($rule)) {
        chomp($rule);
        print "                <tr>\n";
        print "                    <td><font color=\"red\">$line:</font></td>\n";
        print "                    <td>$rule</td>\n";
        print "                    <td></td>\n";
        print "                    <td></td>\n";
        print "                    <td></td>\n";
        print "                    <td></td>\n";
        print "                    <td></td>\n";
        print "                    <td></td>\n";
        print "                    <td></td>\n";
        print "                    <td></td>\n";
        print "                    <td></td>\n";
        print "                    <td><a href=\"edit.t?$sname=$sid&cmd=edit&num=$line&action=comment\">e</a></td>\n";
        print "                    <td><a href=\"edit.t?$sname=$sid&cmd=delete&num=$line\">d</a></td>\n";
        print "                </tr>\n";
    }
    elsif (&macro($rule)) {
        chomp($rule);
        print "                <tr>\n";
        print "                    <td><font color=\"red\">$line:</font></td>\n";
        print "                    <td>$rule</td>\n";
        print "                    <td></td>\n";
        print "                    <td></td>\n";
        print "                    <td></td>\n";
        print "                    <td></td>\n";
        print "                    <td></td>\n";
        print "                    <td></td>\n";
        print "                    <td></td>\n";
        print "                    <td></td>\n";
        print "                    <td><a href=\"edit.t?$sname=$sid&cmd=edit&num=$line&action=macro&comment=$rule\">e</a></td>\n";
        print "                    <td><a href=\"edit.t?$sname=$sid&cmd=delete&num=$line\">d</a></td>\n";
        print "                </tr>\n";
    }
    else {
        # get the 1st element to quickly decide what to do
        my @rule_array = split(/ /,$_[0]);
        $action = shift(@rule_array);
        if (($action eq "pass") || ($action eq "block")) {
            # look for "quick" word in the rule
            if ($rule =~ m/quick/) {
                $quick = "quick";
            }
            if ($rule =~ m/return-rst/) {
                $return = "return-rst";
            }
            elsif ($rule =~ m/return-icmp/) {
                $return = "return-icmp";
            }
            else {
                $return = "";
            }
            # if we have return defined, then the direction will be the 3rd element in the array
            if ($return) {
                my $tmp = shift(@rule_array);
                $direction = shift(@rule_array);
                $tmp = "";
            }
            else {
                $direction = shift(@rule_array);
            }
            # look for protocol info
            if ($rule =~ m/proto/) {
                if ($rule =~ m/tcp/) {
                    $tcp = "tcp";
                }
                if ($rule =~ m/udp/) {
                    $udp = "udp";
                }
                if ($tcp && $udp) {
                    $proto = "{tcp,udp}";
                }
                elsif ($tcp && !$udp) {
                    $proto = "tcp";
                }
                else {
                    $proto = "udp";
                }
            }
            else {
                $proto = "all";
            }
            # get the from, port from, to and port to info by walking threw the array
            my $size = @rule_array;
            # if the rule is normal
            if ($size > 5) {
                for (my $i=0; $i<$size; $i++) {
                    if ($rule_array[$i] eq "from") {
                        $from = $rule_array[$i+1];
                        if ($rule_array[$i+2] eq "port") {
                            $port_from = $rule_array[$i+3];
                        }
                    }
                    if ($rule_array[$i] eq "to") {
                        $to = $rule_array[$i+1];
                        if ($rule_array[$i+2] eq "port") {
                            $port_to = $rule_array[$i+3];
                        }
                    }
                    if ($rule_array[$i] eq "flags") {
                        $flags = $rule_array[$i+1];
                    }
                }
            }
            # if the rule is short. like just pass all traffic
            else {
                $direction = shift(@rule_array);
            }
            # print the output
            print "                <tr>\n";
            print "                    <td><font color=\"red\">$line:</font></td>\n";
            print "                    <td>$action</td>\n";
            print "                    <td>$return</td>\n";
            print "                    <td>$direction</td>\n";
            print "                    <td>$quick</td>\n";
            print "                    <td>$proto</td>\n";
            print "                    <td>$from</td>\n";
            print "                    <td>$port_from</td>\n";
            print "                    <td>$to</td>\n";
            print "                    <td>$port_to</td>\n";
            print "                    <td>$flags</td>\n";
            print "                    <td><a href=\"edit.t?$sname=$sid&cmd=edit&num=$line&action=$action&return=$return&direction=$direction&quick=$quick&proto=$proto&from=$from&port_from=$port_from&to=$to&port_to=$port_to\">e</a></td>\n";
            print "                    <td><a href=\"edit.t?$sname=$sid&cmd=delete&num=$line\">d</a></td>\n";
            print "                </tr>\n";
        }
        elsif ($action eq "scrub") {
            $direction = shift(@rule_array);
            my ($flag1, $flag2);
            if ($rule =~ m/random-id/) {
                $flag1 = "random-id";
            }
            if ($rule =~ m/no-df/) {
                $flag2 = "no-df";
            }
            
            print "                <tr>\n";
            print "                    <td><font color=\"red\">$line:</font></td>\n";
            print "                    <td>$action</td>\n";
            print "                    <td>$return</td>\n";
            print "                    <td>$direction</td>\n";
            print "                    <td>$quick</td>\n";
            print "                    <td>$proto</td>\n";
            print "                    <td>$from</td>\n";
            print "                    <td>$port_from</td>\n";
            print "                    <td>$to</td>\n";
            print "                    <td>$port_to</td>\n";
            print "                    <td>$flag1 $flag2</td>\n";
            print "                    <td><a href=\"edit.t?$sname=$sid&cmd=edit&num=$line&action=$action&direction=$direction&flag1=$flag1&flag2=$flag2\">e</a></td>\n";
            print "                    <td><a href=\"edit.t?$sname=$sid&cmd=delete&num=$line\">d</a></td>\n";
            print "                </tr>\n";
        }
        elsif ($action eq "set") {
            # set
            print "                <tr>\n";
            print "                    <td><font color=\"red\">$line:</font></td>\n";
            print "                    <td>$rule</td>\n";
            print "                    <td></td>\n";
            print "                    <td></td>\n";
            print "                    <td></td>\n";
            print "                    <td></td>\n";
            print "                    <td></td>\n";
            print "                    <td></td>\n";
            print "                    <td></td>\n";
            print "                    <td></td>\n";
            print "                    <td></td>\n";
            print "                    <td><a href=\"edit.t?$sname=$sid&cmd=edit&num=$line&action=$action\">e</a></td>\n";
            print "                    <td><a href=\"edit.t?$sname=$sid&cmd=delete&num=$line\">d</a></td>\n";
            print "                </tr>\n";
        }
        else {
            # empty line
            print "                <tr>\n";
            print "                    <td><font color=\"red\">$line:</font></td>\n";
            print "                    <td></td>\n";
            print "                    <td></td>\n";
            print "                    <td></td>\n";
            print "                    <td></td>\n";
            print "                    <td></td>\n";
            print "                    <td></td>\n";
            print "                    <td></td>\n";
            print "                    <td></td>\n";
            print "                    <td></td>\n";
            print "                    <td></td>\n";
            print "                    <td><a href=\"edit.t?$sname=$sid&cmd=edit&num=$line&action=empty\">e</a></td>\n";
            print "                    <td><a href=\"edit.t?$sname=$sid&cmd=delete&num=$line\">d</a></td>\n";
            print "                </tr>\n";   
        }
    }

}

sub redirect {
    my $address = $_[0];
    print "<html>\n<head>\n";
    print "<meta http-equiv=\"REFRESH\" content=\"0;url=$address\">\n";
    print "</head>\n<body>\n";
    print "</body>\n</html>";
}

exit 0;