#!/usr/bin/perl -w

# Copyright (c) 2007-2008 Artjom Vassiljev <me@al1us.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use strict;

use CGI;
use CGI::Session qw/-ip-match/;

my $cgi = CGI->new();
my $title = "Add rule";

my $sid = $cgi->param("CGISESSID") || undef;
my $session = new CGI::Session(undef, $sid, {Directory=>"/tmp"});
my $sname = $session->name;
my $page = $cgi->param("page");

# let's check whether the user is logged in or not
my $logged = $session->param("logged");
if (!$logged) { &redirect("/start.t?$sname=$sid"); }
else {
    
    &html_header();
    
    if ($page eq "pass") {
        &pass();
    }
    elsif ($page eq "block") {
        &block();
    }
    elsif ($page eq "quick") {
        &quick();
    }
    else {
        &pass();
    }
    
    &html_footer();
}

sub html_header {
    print "<html>\n";
    print "<head>\n";
    print " <title>$title</title>\n";
    print " <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/main.css\" media=\"screen\" />\n";
    print "</head>\n";
    print "<body>\n";
    print "<div id=\"global\">\n";
    print " <div id=\"mainmenu\">\n";
    print "  <ul>\n";
    if ($page eq "pass") {
        print "     <li class=\"active\"><a href=\"/modules/pf/form_add.t?$sname=$sid&page=pass\" class=\"active\">pass</a></li>\n";
    }
    else {
        print "     <li><a href=\"/modules/pf/form_add.t?$sname=$sid&page=pass\">pass</a></li>\n";
    }
    if ($page eq "block") {
        print "     <li class=\"active\"><a href=\"/modules/pf/form_add.t?$sname=$sid&page=block\" class=\"active\">block</a></li>\n";
    }
    else {
        print "     <li><a href=\"/modules/pf/form_add.t?$sname=$sid&page=block\">block</a></li>\n";
    }
    if ($page eq "quick") {
        print "     <li class=\"active\"><a href=\"/modules/pf/form_add.t?$sname=$sid&page=quick\" class=\"active\">quick</a></li>\n";
    }
    else {
        print "     <li><a href=\"/modules/pf/form_add.t?$sname=$sid&page=quick\">quick</a></li>\n";
    }
    print "  </ul>\n";
    print " </div>\n";
    print " <br /><br />\n";
    print " <div id=\"content\">\n";
}

sub html_footer {
    print "</div>\n";
    print "</div>\n";
    print "</body>\n";
    print "</html>\n";
}

sub redirect {
    my $address = $_[0];
    print "<html>\n<head>\n";
    print "<meta http-equiv=\"REFRESH\" content=\"0;url=$address\">\n";
    print "</head>\n<body>\n";
    print "</body>\n</html>";
}

sub pass {
    print "       <form action=\"add.t?$sname=$sid\" method=\"post\">\n";
    print "          <table border=\"1\">\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      Print to line number<br />(if no value, then prints to the end):\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      <input type=\"text\" name=\"num\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      Let the traffic in or out:\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      <input type=\"hidden\" name=\"action\" value=\"pass\">\n";
    print "                      <select name=\"direction\">\n";
    print "                          <option>in</option>\n";
    print "                          <option>out</option>\n";
    print "                      </select>\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      Pass in quickly or proceed to the end of the list\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      <select name=\"quick\">\n";
    print "                          <option></option>\n";
    print "                          <option>quick</option>\n";
    print "                      </select>\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      protocol:\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      all:<input type=\"checkbox\" name=\"protocol\" value=\"all\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      tcp:<input type=\"checkbox\" name=\"protocol\" value=\"tcp\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      udp:<input type=\"checkbox\" name=\"protocol\" value=\"udp\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      from:\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      <input type=\"text\" name=\"from\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      port:\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      <input type=\"text\" name=\"port_from\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      to:\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      <input type=\"text\" name=\"to\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      port:\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      <input type=\"text\" name=\"port_to\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      <input type=\"submit\" value=\"ok\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "          </table>\n";
    print "       </form>\n";
}

sub block {
    print "       <form action=\"add.t?$sname=$sid\" method=\"post\">\n";
    print "          <table border=\"1\">\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      Print to line number<br />(if no value, then prints to the end):\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      <input type=\"text\" name=\"num\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      Be polite. Return RESET packet to client\n";
    print "                      <input type=\"hidden\" name=\"action\" value=\"block\">\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      <select name=\"return\">\n";
    print "                          <option></option>\n";
    print "                          <option>return-rst</option>\n";
    print "                          <option>return-icmp</option>\n";
    print "                      </select>\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      Let the traffic in or out:\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      <select name=\"direction\">\n";
    print "                          <option>in</option>\n";
    print "                          <option>out</option>\n";
    print "                      </select>\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      Pass in quickly or proceed to the end of the list\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      <select name=\"quick\">\n";
    print "                          <option></option>\n";
    print "                          <option>quick</option>\n";
    print "                      </select>";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      protocol:<br />\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      all: <input type=\"checkbox\" name=\"protocol\" value=\"all\">\n";
    print "                      tcp: <input type=\"checkbox\" name=\"protocol\" value=\"tcp\">\n";
    print "                      udp: <input type=\"checkbox\" name=\"protocol\" value=\"udp\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      from:\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      <input type=\"text\" name=\"from\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      port:\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      <input type=\"text\" name=\"port_from\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      to:\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      <input type=\"text\" name=\"to\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      port:\n";
    print "                  </td>\n";
    print "                  <td>\n";
    print "                      <input type=\"text\" name=\"port_to\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      <input type=\"submit\" value=\"ok\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "          </table>\n";
    print "       </form>\n";
}

sub quick {
    print "       <form action=\"add.t?$sname=$sid\" method=\"post\">\n";
    print "          <table border=\"1\">\n";
    print "              <tr>\n";
    print "                  <td>Pass or block all incoming traffic:</td>";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>";
    print "                      Line number (default: prints to the end of file!):<br />\n";
    print "                      <input type=\"text\" name=\"num\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      <input type=\"hidden\" name=\"q\" value=\"short\">\n";
    print "                      <select name=\"action\">\n";
    print "                          <option>pass</option>\n";
    print "                          <option>block</option>\n";
    print "                      </select>\n";
    print "                  </td>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      <select name=\"direction\">\n";
    print "                          <option>in</option>\n";
    print "                          <option>out</option>\n";
    print "                      </select>\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "              <tr>\n";
    print "                  <td>\n";
    print "                      <input type=\"submit\" value=\"ok\">\n";
    print "                  </td>\n";
    print "              </tr>\n";
    print "          </table>";
    print "       </form><br /><br />\n";
}

exit 0;