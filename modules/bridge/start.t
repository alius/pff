#!/usr/bin/perl -w

# Copyright (c) 2007-2008 Artjom Vassiljev <me@al1us.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use strict;

use CGI;
use CGI::Session qw/-ip-match/;;

# variables

my $cgi = CGI->new();
my $sid = $cgi->param("CGISESSID") || undef;
my $session = new CGI::Session(undef, $sid, {Directory=>"/tmp"});
my $sname = $session->name;

# let's check whether the user is logged in or not
my $logged = $session->param("logged");
if (!$logged) { &redirect("/start.t?$sname=$sid"); }
else {
    &html_header();   
}

sub html_header {
    print "Conent-type: text/html\n\n";
    print "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
    print "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n";
    print "<head>\n";
    print "    <title>Bridge</title>\n";
    print "    <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\n";
    print "    <link rel=\"shortcut icon\" href=\"/images/favicon.ico\" />\n";
    print "    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/main.css\" media=\"screen\" />\n";
    print "    <script type=\"text/javascript\" src=\"/js/jquery.js\"></script>\n";
    print "</head>\n";
    print "<body>\n";
    print "    <div id=\"global\">\n";
    print "        <div id=\"header\">\n";
    print "<form method=\"post\" action=\"\" style=\"float:right\">\n";
    print " <input type=\"hidden\" name=\"cmd\" value=\"logout\">\n";
    print " <input type=\"submit\" value=\"logout\">\n";
    print "</form>\n";
    print "            <span class=\"title\">BRIDGING FIREWALL.</span>\n";
    print "            <span class=\"sub_title\">The invisible security</span>\n";
    print "        </div>\n";
    print "        <div id=\"mainmenu\">\n";
    print "            <ul>\n";
    print "                <li><a href=\"#\">Bridge</a></li>\n";
    print "                <li><a href=\"/modules/pf/start.t?$sname=$sid\" class=\"active\">Packet filter</a></li>\n";
    print "            </ul>\n";
    print "        </div>\n";            
    print "        <div id=\"content\">\n";
    print "            <div id=\"sub_section\">\n";
    print "                <a href=\"add.t\" class=\"buttons\">add</a>\n";
    print "            </div>\n";
    print "            <h2>Bridge info</h2>\n";
    print "            <table>\n";
    print "                <thead>\n";
    print "                    <tr class=\"top\">\n";
    print "                        <td>Bridge status</td>\n";
    print "                        <td>Learned addresses</td>\n";
    print "                    </tr>\n";
    print "                </thead>\n";
    print "                <tr>\n";
    &bridge_info();
    &bridge_addresses();
    print "                </tr>\n";
    print "            </table>\n";
    print "            <script>\n";
    print "                \$(\"tr:nth-child(odd)\").addClass(\"dark\");\n";
    print "            </script>\n";
    print "        </div>\n";
    print "        <div id=\"sub_section\">\n";
    print "            <a href=\"add.t\" class=\"buttons\">add</a>\n";
    print "        </div>\n";
    print "    </div>\n";
    print "</body>\n";
    print "</html>\n";
}

sub redirect {
    my $address = $_[0];
    print "<html>\n<head>\n";
    print "<meta http-equiv=\"REFRESH\" content=\"0;url=$address\">\n";
    print "</head>\n<body>\n";
    print "</body>\n</html>";
}

sub bridge_info {
    system("brconfig -A > /tmp/out.txt");
    open(BR_STATUS, "/tmp/out.txt");
    my @a = <BR_STATUS>;
    close(BR_STATUS);
    unlink("/tmp/out.txt");
    print "                    <td>\n";
    my $count = 0;
    foreach(@a) {
        if($_ =~m/</) {
            $_ =~s/</&lt;/
        }
        if($_ =~m/>/) {
            $_ =~s/>/&gt;/
        }
        $_ = $_ . "<br />";
        if ($count > 0) {
            $_ = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $_;
        }
        
        print "                    $_\n";
        $count++;
    }
    print "                    </td>\n";
}

sub bridge_addresses {
    system("brconfig bridge0 addr");
    open(BR_STATUS, "/tmp/out.txt");
    my @a = <BR_STATUS>;
    close(BR_STATUS);
    unlink("/tmp/out.txt");
    print "                    <td>\n";
    foreach(@a) {
        print "                    $_\n";
    }
    print "                    </td>\n";
}

exit 0