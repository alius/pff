#!/usr/bin/perl -w

# Copyright (c) 2007-2008 Artjom Vassiljev <me@al1us.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use lib 'classes/';
use pff;

use strict;
use CGI;
use CGI::Session qw/-ip-match/;

my $pf2 = pff->new();
my $cgi = CGI->new();
my $sid = $cgi->param("CGISESSID");
my $session = new CGI::Session(undef, $sid, {Directory=>"/tmp"});
my $sname = $session->name;
my $version = "2.0 alpha";
my $cmd = $cgi->param("cmd");

if (($cmd) && ($cmd eq "logout")) {
      &logout();
      exit 0;
}

# let's check whether the user is logged in or not
my $logged = $session->param("logged");
if ($logged != 1) { $pf2->redirect("/start.t") }
else {
# start html
   print "Content-type: text/html\n\n";
   print "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
   print "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n<head>\n";
   print "<title>Dashboard</title>\n";
   print "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\n";
   print "<link rel=\"shortcut icon\" href=\"/images/favicon.ico\" />\n";
   print "<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/main.css\" media=\"screen\" />\n";
   print "<script type=\"text/javascript\" src=\"/js/jquery.js\"></script>\n";
   print "</head>\n<body>\n";
   print "<div id=\"global\">\n";
   print "<div id=\"header\">\n";
   print "<form method=\"post\" action=\"\" style=\"float:right\">\n";
   print " <input type=\"hidden\" name=\"cmd\" value=\"logout\">\n";
   print " <input type=\"submit\" value=\"logout\">\n";
   print "</form>\n";
   print "<span class=\"title\">BRIDGING FIREWALL.</span>\n";
   print "<span class=\"sub_title\">The invisible security</span>\n";
   print "</div>\n";
   print "     <div id=\"mainmenu\">\n";
   print "         <ul>\n";
# subroutine to find modules in the module directory
   $pf2->module_find('0', $sname, $sid, '             ');
   print "         </ul>\n";
   print "     </div>\n";

# subroutine to print time, date, interface info, etc
   my $a = &system_info();
   print "         <h2>Dashboard</h2>\n";
   print "         <table>\n";
   print "             <thead>\n";
   print "                 <tr class=\"top\">\n";
   print "                     <td>#</td>\n";
   print "                     <td>Version:</td>\n";
   print "                     <td>System info:</td>\n";
   print "                 </tr>\n";
   print "             </thead>\n";
   print "             <tr>\n";
   print "                 <td></td>\n";
   print "                 <td>$version</td>\n";
   print "                 <td>$a</td>\n";
   print "             </tr>\n";
   print "            </table>\n";
   print "         <script>\n";
   print "             \$(\"tr:nth-child(odd)\").addClass(\"dark\")\;\n";
   print "         </script>\n";
   print "     </div>\n";

   print " </div>\n";
   print "</body>\n";
   print "</html>\n";

}

# functions

sub system_info {
# time and date
   my $time = `date`;
   chomp($time);
   return($time);
}

sub logout {
      $session->delete();
      &redirect();
}

exit 0;
